;*
;*
;* Copyright (C) 2016 Texas Instruments Incorporated - http://www.ti.com/
;*
;*
;*  Redistribution and use in source and binary forms, with or without
;*  modification, are permitted provided that the following conditions
;*  are met:
;*
;*    Redistributions of source code must retain the above copyright
;*    notice, this list of conditions and the following disclaimer.
;*
;*    Redistributions in binary form must reproduce the above copyright
;*    notice, this list of conditions and the following disclaimer in the 
;*    documentation and/or other materials provided with the
;*    distribution.
;*
;*    Neither the name of Texas Instruments Incorporated nor the names of
;*    its contributors may be used to endorse or promote products derived
;*    from this software without specific prior written permission.
;*
;*
;*
;*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS OR
;*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
;*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
;*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
;*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
;*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
;*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
;*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;*

        .if PRUSS_V2 = 1
ARM_PRU0_EVENT  .set 16
PRU0_ARM_EVENT  .set 17
ARM_PRU1_EVENT  .set 18
PRU1_ARM_EVENT  .set 19
        .else
ARM_PRU0_EVENT  .set 32 
PRU0_ARM_EVENT  .set 33
ARM_PRU1_EVENT  .set 34
PRU1_ARM_EVENT  .set 35
        .endif

MOV32   .macro dst, src
        LDI     dst.w0, src & 0xFFFF
        LDI     dst.w2, src >> 16
        .endm

        .def ARM_TO_PRU_INTERRUPT_POLL
 
ARM_TO_PRU_INTERRUPT_POLL:
;  Poll for receipt of interrupt on host 0
        .if CORE_PRU = 0
        QBBS      EVENT, R31, 30
        .endif
        .if CORE_PRU = 1
        QBBS      EVENT, R31, 31
        .endif
        JMP       ARM_TO_PRU_INTERRUPT_POLL

EVENT:
; Clear system event in SECR1/2
        MOV32   r0, 0
; Send notification to Host for program completion
        .if PRUSS_V2 = 1
        LDI     r3.w0, 0x280 ; SECR1
        .if     CORE_PRU = 0
        SET     r0, r0, ARM_PRU0_EVENT
        LDI     R31.b0, PRU0_ARM_EVENT+16
        .endif
        .if     CORE_PRU = 1
        SET     r0, r0, ARM_PRU1_EVENT
        LDI     R31.b0, PRU1_ARM_EVENT+16
        .endif
        .else
        LDI     r3.w0, 0x284 ; SECR2
        .if     CORE_PRU = 0
        SET     r0, r0, ARM_PRU0_EVENT-32
        LDI     R31.b0, PRU0_ARM_EVENT
        .endif
        .if     CORE_PRU = 1
        SET     r0, r0, ARM_PRU1_EVENT-32
        LDI     R31.b0, PRU1_ARM_EVENT
        .endif
        .endif
        SBCO    &r0, C0, r3.w0, 4 
        QBA ARM_TO_PRU_INTERRUPT_POLL
